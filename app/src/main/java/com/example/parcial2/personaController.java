package com.example.parcial2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Date;

public class personaController {
    private BaseDatos db;
    Date fecha = new Date();
    public personaController(Context context) {
        this.db = new BaseDatos(context);
    }

    public long agregarPersona(Persona e){
        try{

            SQLiteDatabase database = db.getWritableDatabase();
            ContentValues valores = new ContentValues();
            valores.put(DefDB.col_cedula,e.getCedula());
            valores.put(DefDB.col_nombre,e.getNombre());
            valores.put(DefDB.col_salario,e.getSalario());
            valores.put(DefDB.col_nivelEducativo,e.getNivelEducativo());
            valores.put(DefDB.col_estrato,e.getEstrato());


            long id = database.insert(DefDB.tabla_est,null,valores);
            return id;
        }
        catch (Exception ex){
            System.out.println("Error al insertar");
            return 0;
        }
    }

    public boolean buscarVisitor(String cod){
        String[] args = new String[] {cod};
        SQLiteDatabase database = db.getReadableDatabase();
        Cursor c = database.query(DefDB.tabla_est, null, "cedula=?", args, null, null, null);
        if (c.getCount()>0) {
            database.close();
            return true;
        }
        else{
            database.close();
            return false;}

    }

    public long actualizarPersona(Persona e){

        try{
            SQLiteDatabase database = db.getWritableDatabase();
            ContentValues valores = new ContentValues();
            String[] args = new String[] {String.valueOf(e.getCedula())};
            valores.put(DefDB.col_nombre,e.getNombre());
            valores.put(DefDB.col_estrato,e.getEstrato());
            valores.put(DefDB.col_nivelEducativo,e.getNivelEducativo());
            valores.put(DefDB.col_salario,e.getSalario());
            long id = database.update(DefDB.tabla_est, valores,"cedula=?",args);
            database.close();
            return id;
        }
        catch (Exception ex){
            System.out.println("Error al actualizar");
            return 0;
        }

    }
    public Cursor allPersons() {
        try {
            SQLiteDatabase database = db.getWritableDatabase();
            Cursor cur = database.rawQuery("select cedula as _id,* from persona", null);
            return cur;
        } catch (Exception ex) {
            System.out.println("Error al consultar");
            return null;
        }
    }
    public boolean deletePersona(String cod){
        try{
            SQLiteDatabase database = db.getWritableDatabase();

            String[] args = new String[] {cod};

            long id = database.delete(DefDB.tabla_est,"cedula=?",args);
            database.close();
            return true;
        }
        catch (Exception ex){
            System.out.println("Error al eliminar");
            return false;
        }
    }
    public Persona findVisitor(String cedula){
        String[] args = new String[] {cedula};
        SQLiteDatabase database = db.getReadableDatabase();
        Cursor c = database.query(DefDB.tabla_est, null, "cedula=?", args, null, null, null);
        if (c.getCount()>0) {
            c.moveToFirst();
            Persona visitor = new Persona(
                    c.getString(c.getColumnIndexOrThrow(DefDB.col_cedula)),
                    c.getString(c.getColumnIndexOrThrow(DefDB.col_nombre)),
                    c.getString(c.getColumnIndexOrThrow(DefDB.col_estrato)),
                    c.getDouble(c.getColumnIndexOrThrow(DefDB.col_salario)),
                    c.getString(c.getColumnIndexOrThrow(DefDB.col_nivelEducativo)));
            c.close();
            database.close();
            return visitor;
        }
        else{
            database.close();
            return null;}
    }
}
