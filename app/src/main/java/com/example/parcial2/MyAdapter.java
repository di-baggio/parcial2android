package com.example.parcial2;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class MyAdapter extends CursorAdapter {

    Context context;
    int layoutResourceId;
    Cursor data = null;

    public MyAdapter(Context context, Cursor data,int layoutResourceId) {
        super(context, data,layoutResourceId);

        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(layoutResourceId, parent, false);
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        MyAdapterHolder holder = new MyAdapterHolder();
        holder.cedula = (TextView) view.findViewById(R.id.txtCedulaPersona);
        holder.nombre = (TextView) view.findViewById(R.id.txtNombrePersona);
        holder.estrato = (TextView) view.findViewById(R.id.txtEstratoPersona);
        holder.salario = (TextView) view.findViewById(R.id.txtSalarioPersona);
        holder.nivel = (TextView) view.findViewById(R.id.txtNivelPersona);

        holder.cedula.setText(cursor.getString(cursor.getColumnIndexOrThrow(DefDB.col_cedula)));
        holder.nombre.setText(cursor.getString(cursor.getColumnIndexOrThrow(DefDB.col_nombre)));
        holder.estrato.setText("Estrato socioeconomico "+cursor.getString(cursor.getColumnIndexOrThrow(DefDB.col_estrato)));
        holder.salario.setText("Salario: "+cursor.getString(cursor.getColumnIndexOrThrow(DefDB.col_salario)));
        holder.nivel.setText("Nivel educativo: "+cursor.getString(cursor.getColumnIndexOrThrow(DefDB.col_nivelEducativo)));
    }


    static class MyAdapterHolder {
        TextView cedula;
        TextView nombre;
        TextView estrato;
        TextView salario;
        TextView nivel;
    }
}
