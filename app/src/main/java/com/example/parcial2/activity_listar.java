package com.example.parcial2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;

public class activity_listar extends AppCompatActivity {
    private ListView lista;
    private ArrayList<Persona> listaPersonas = new ArrayList<>();
    Cursor personas;
    MyAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);
        lista = findViewById(R.id.ltPersonas);
        final personaController PersonaController = new personaController(getApplicationContext());
        personas = PersonaController.allPersons();

       adapter = new MyAdapter(this,personas,R.layout.personas);
        lista.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position,
                                    long id) {
                Log.i("",parent.getItemAtPosition(position).toString());
                final PopupMenu popupMenu = new PopupMenu(activity_listar.this,findViewById(R.id.txtNombrePersona), Gravity.CENTER_HORIZONTAL);
                popupMenu.getMenuInflater().inflate(R.menu.menu_popup,popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();

                        switch (id) {
                            case R.id.mnuEditar:
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra("cedula", personas.getString(personas.getColumnIndexOrThrow(DefDB.col_cedula)));
                                startActivity(i);
                                finish();
                                break;
                            case R.id.mnuEliminar:
                                PersonaController.deletePersona(personas.getString(personas.getColumnIndexOrThrow(DefDB.col_cedula)));
                                personas = PersonaController.allPersons();

                                adapter = new MyAdapter(getApplicationContext(),personas,R.layout.personas);
                                lista.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                break;
                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.mnuAgregar) {
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
