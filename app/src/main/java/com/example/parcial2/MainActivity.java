package com.example.parcial2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button btnGrabar, btnCancelar;
    private EditText etCedula,etNombre,etSalario;
    private Spinner spnEstrato,spnNivel;
    private ArrayAdapter adapterEstrato,adapterNivel;
    private ArrayList<String> arrayEstrato,arrayNivel;
    private Boolean valorActualizar = false;
    private personaController PersonaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PersonaController = new personaController(getApplicationContext());
        arrayEstrato = new ArrayList<>();
        arrayNivel = new ArrayList<>();

        etCedula = findViewById(R.id.edtCedula);
        etNombre = findViewById(R.id.edtNombre);
        etSalario = findViewById(R.id.edtSalario);
        spnEstrato = findViewById(R.id.spnEstrato);
        spnNivel = findViewById(R.id.spnNivel);
        btnGrabar = findViewById(R.id.btnGrabar);
        btnCancelar = findViewById(R.id.btnCancelar);

        arrayEstrato.add("1");
        arrayEstrato.add("2");
        arrayEstrato.add("3");
        arrayEstrato.add("4");
        arrayEstrato.add("5");
        arrayEstrato.add("6");

        arrayNivel.add("Bachillerato");
        arrayNivel.add("Pregrado");
        arrayNivel.add("Maestría");
        arrayNivel.add("Doctorado");

        adapterEstrato = new ArrayAdapter(getApplicationContext(),R.layout.support_simple_spinner_dropdown_item,arrayEstrato);
        spnEstrato.setAdapter(adapterEstrato);
        adapterEstrato.notifyDataSetChanged();

        adapterNivel = new ArrayAdapter(getApplicationContext(),R.layout.support_simple_spinner_dropdown_item,arrayNivel);
        spnNivel.setAdapter(adapterNivel);
        adapterNivel.notifyDataSetChanged();

        Intent intent = getIntent();
        if(intent != null){
            Bundle bundle = intent.getExtras();
            if (bundle != null){
                String cedula = bundle.getString("cedula");
                if(cedula != null) {
                    Persona persona = PersonaController.findVisitor(cedula);
                    etCedula.setText(persona.getCedula());
                    etNombre.setText(persona.getNombre());
                    etSalario.setText(String.valueOf(persona.getSalario()));

                    buscarReemplazarSpinner(persona.getEstrato(), spnEstrato, arrayEstrato);
                    buscarReemplazarSpinner(persona.getNivelEducativo(), spnNivel, arrayNivel);
                    this.valorActualizar = true;
                }
            }
        }

        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etCedula.getText().toString().trim().isEmpty() || etNombre.getText().toString().trim().isEmpty() ||
                        etSalario.getText().toString().trim().isEmpty() ||
                spnEstrato.getSelectedItem().toString() == "" || spnNivel.getSelectedItem().toString() == ""){

                   if(etCedula.getText().toString().trim().isEmpty()){
                       etCedula.setError("Este campo es obligatorio");
                   }
                    if(etNombre.getText().toString().trim().isEmpty()){
                        etNombre.setError("Este campo es obligatorio");
                    }
                    if(etSalario.getText().toString().trim().isEmpty()){
                        etSalario.setError("Este campo es obligatorio");
                    }
                    if(spnEstrato.getSelectedItem().toString()==""){
                        Toast.makeText(getApplicationContext(),"Debe seleccionar un estrato",Toast.LENGTH_SHORT).show();
                    }
                    if(spnNivel.getSelectedItem().toString() == ""){
                        Toast.makeText(getApplicationContext(),"Debe seleccionar un nivel educativo",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if(valorActualizar){
                        actualizar();
                    }else{
                        grabar();
                    }
                    Intent intent = new Intent(getApplicationContext(),activity_listar.class);
                    startActivity(intent);
                    finish();

                }
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void grabar(){
        Persona persona = new Persona(etCedula.getText().toString(),etNombre.getText().toString(),spnEstrato.getSelectedItem().toString(),
               Double.parseDouble(etSalario.getText().toString().trim()),spnNivel.getSelectedItem().toString());
        PersonaController.agregarPersona(persona);
    }
    private void actualizar(){
        Persona persona = new Persona(etCedula.getText().toString(),etNombre.getText().toString(),spnEstrato.getSelectedItem().toString(),
                Double.parseDouble(etSalario.getText().toString().trim()),spnNivel.getSelectedItem().toString());
        PersonaController.actualizarPersona(persona);
    }

    private void buscarReemplazarSpinner(String valor, Spinner spnObjeto, ArrayList<String> lista){
        for(int i=0; i<lista.size();i++){
            if(lista.get(i).trim().equalsIgnoreCase(valor.trim())){
                spnObjeto.setSelection(i);
                break;
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.mnuLista) {
            Intent intent = new Intent(getApplicationContext(),activity_listar.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
