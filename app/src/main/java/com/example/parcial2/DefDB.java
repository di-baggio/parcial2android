package com.example.parcial2;

public class DefDB {

    public static final String nameDb = "parcialUAC.db";
    public static final String tabla_est    = "persona";
    public static final String col_cedula   = "cedula";
    public static final String col_nombre   = "nombre";
    public static final String col_estrato  = "estrato";
    public static final String col_salario  = "salario";
    public static final String col_nivelEducativo ="nivel_educativo";

    public static final String create_tabla_est = "CREATE TABLE persona (" +
            "  cedula varchar(15) primary key," +
            "  nombre text," +
            "  estrato varchar(20)," +
            "  salario numeric(18,2),"+
            "  nivel_educativo text  "+
            ");";
}
