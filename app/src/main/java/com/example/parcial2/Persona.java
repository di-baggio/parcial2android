package com.example.parcial2;

import android.os.Parcel;
import android.os.Parcelable;



public class Persona implements Parcelable {
    private String cedula;
    private String nombre;
    private String estrato;
    private double salario;
    private String nivelEducativo;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstrato() {
        return estrato;
    }

    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getNivelEducativo() {
        return nivelEducativo;
    }

    public void setNivelEducativo(String nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }

    public Persona(Parcel in) {
        this.cedula = in.readString();
        this.nombre = in.readString();
        this.estrato = in.readString();
        this.salario = in.readDouble();
        this.nivelEducativo = in.readString();
    }

    public Persona(String cedula, String nombre, String estrato, double salario, String nivelEducativo) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.estrato = estrato;
        this.salario = salario;
        this.nivelEducativo = nivelEducativo;
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cedula);
        dest.writeString(this.nombre);
        dest.writeString(this.estrato);
        dest.writeDouble(this.salario);
        dest.writeString(this.nivelEducativo);
    }
}
